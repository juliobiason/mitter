from mitterlib.constants import version

params = {
        'name': 'mitter',
        'version': version,
        'description': 'Update your Twitter status',
        'author': 'Julio Biason',
        'author_email': 'julio@juliobiason.net',
        'url': 'http://code.google.com/p/mitter/',
        'scripts': ['mitter'],
        'packages': [
            'mitterlib',
            'mitterlib.ui',
            'mitterlib.ui.helpers',
            'mitterlib.network'],
        'data_files': [
            ('share/pixmaps',
                ['pixmaps/mitter.png',
                 'pixmaps/mitter-big.png',
                 'pixmaps/mitter-new.png',
                 'pixmaps/mitter-error.png',
                 'pixmaps/unknown.png']),
            ('share/locale/en_US/LC_MESSAGES',
                ['locale/en_US/LC_MESSAGES/mitter.mo']),
            ('share/applications',
                ['mitter.desktop'])],
        'license': 'GPL3',
        'download_url': \
            'http://mitter.googlecode.com/files/mitter-%s.tar.gz' % (version),
        'classifiers': [
            'Development Status :: 4 - Beta',
            'Environment :: Console',
            'Environment :: X11 Applications :: GTK',
            'Intended Audience :: End Users/Desktop',
            'License :: OSI Approved :: GNU General Public License (GPL)',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Topic :: Communications :: Chat']}

from distutils.core import setup
from distutils.version import StrictVersion

import sys
version_number = '.'.join([str(a) for a in sys.version_info[:3]])

if StrictVersion(version_number) < StrictVersion('2.6'):
    params['requires'] = ['simplejson']

setup(**params)
