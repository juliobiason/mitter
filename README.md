# Read Me

## About Mitter
Mitter is a multi-protocol, multi-interface client for micro-blogging networks.

The project is currently unmaintained and doesn't support OAuth, mostly due the way Twitter handles secrets and such, preventing me from distributing the code with a proper key and making it pretty hard for the common person to create its own application.

## Options

To get a list of options Mitter support, you can call it with `mitter --help`. Along with the default options, you'll get options for each interface and network.

## Config file

Mitter saves its options in a config file in your home directory, called `.mitter.ini`. Some options, not displayed in the command line options, are available there. Be careful when changing those options or Mitter may stop working. Documentation about those options is available in the beginning of the module and should be available with the generated documentation.

In any case, if things don't seem to be working, you can remove this file and Mitter will ask the necessary options to work again.

## Networks

As for version 1.0, Mitter supports Twitter.

# Interfaces

As for version 1.0, Mitter have the following interfaces: PyGTK, TTY, Cmd, Zork.

## Choosing Interfaces

If you want to chose a specific interface, you can use the `-i` option, followed by the interface name. Once you have chose an interface, Mitter will keep opening it every time it runs. To change to another interface, use the `-i` option again.

### PyGTK

PyGTK is a graphical interface, which uses the GTK toolkit. To be used, it requires that the PyGTK module is installed in your system. If you don't have PyGTK, Mitter will switch to another interface that doesn't require it.

### TTY

TTY is a text interface, used to display the updates. It does not offer any interactivity. You can use it to do automated updates without user intervention or to simply retrieve the current messages.

### Cmd

CMD is a text interface, with a command line. It will display the messages in a numbered list and most of options related to those messages will require the message number. It also have some smart behaviour, like retrieving new messages when there is no option and starting an update when the command is not recognized.

### Zork

Zork is another text interface, but works a little bit slowly than the Cmd interface. Instead of displaying all the messages at once and requiring that you select message by their numbers, Zork displays message by message, requiring the user to jump to the next message. It offers a fine-grained control to which message to reply, at the expense of few commands.

## How Mitter Finds Interfaces

Mitter uses Python default module loading to find interfaces. It works like this: It will search modules starting with the current directory, then the global system path. When installed via `setup.py` or `easy_install`, interfaces will be installed in the global system directory; if you don't want to install Mitter, you can run it in the command line, just be sure to be in the main `mitter` directory before call it.