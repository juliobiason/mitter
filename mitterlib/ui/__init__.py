#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a simple client for Twitter
# Copyright (C) 2007, 2008  The Mitter Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os.path

from mitterlib import module_search

_log = logging.getLogger('mitterlib.ui.Interfaces')

# List of files that are not networks
SKIPPABLES = ('__init__.py')

#--------------------------------------------------------------------
# Helper functions
#--------------------------------------------------------------------

def _import_name(module):
    """Based on the name of the module, return the proper "import"
    statement."""
    (name, _) = os.path.splitext(module)
    if name.startswith('ui_'):
        mask = 'mitterlib.ui.%s'
    else:
        mask = 'mitterlib.ui.ui_%s'

    return mask % (name)


class Interfaces(object):
    """Interface transparency layer: Check which interfaces are available and
    tries to load the interface requested by the user."""

    def __init__(self, options):
        self._interfaces = []
        self._priority_list = {}
        self._options = options
        self.options()
        return

    def options(self):
        """Request all networks to add their options."""
        for module_name in module_search(__file__, SKIPPABLES):
            import_name = _import_name(module_name)

            try:
                _log.debug('Importing module %s', import_name)
                module = __import__(import_name, fromlist=[import_name])

                interface_name = module.Interface.NAMESPACE
                priority = module.Interface.PRIORITY

                module.Interface.options(self._options)

                self._interfaces.append(interface_name)
                if priority in self._priority_list:
                    self._priority_list[priority].append(interface_name)
                else:
                    self._priority_list[priority] = [interface_name]
            except ImportError, exc:
                _log.debug('Cannot import module %s', import_name)
                _log.debug(str(exc))

        return

    def load(self, connection, prefer=None):
        """Start the interface, using the prefered one."""
        if not self._interfaces:
            return None

        if prefer and prefer in self._interfaces:
            import_name = prefer
        else:
            # So we pick the one in the top of the list ('cause we know it's
            # importable and the user didn't chose anything.)
            _log.debug('Priority list: ' + str(self._priority_list))
            priority = sorted(self._priority_list.keys(), reverse=True)
            top_priority = priority[0]
            import_name = self._priority_list[top_priority][0]

        _log.debug('Loading interface %s', import_name)
        module_name = _import_name(import_name)
        module = __import__(module_name, fromlist=[module_name])
        return module.Interface(connection, self._options)
