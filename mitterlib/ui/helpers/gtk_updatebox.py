#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a multiple-interface client for microblogging services.
# Copyright (C) 2007-2010  Mitter Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gobject
import gtk
import logging
import gettext

# ----------------------------------------------------------------------
# Logging
# ----------------------------------------------------------------------
_log = logging.getLogger('ui.pygtk.updatebox')

# ----------------------------------------------------------------------
# I18n bits
# ----------------------------------------------------------------------
t = gettext.translation('ui_pygtk', fallback=True)
# Still mark translations inside ui_pygtk
_ = t.gettext
N_ = t.ngettext

# ----------------------------------------------------------------------
# UpdateBox class
# ----------------------------------------------------------------------

class UpdateBox(gtk.VBox):
    """Custom update box widget."""

    __gsignals__ = {
            'text-focus': (
                gobject.SIGNAL_RUN_LAST,
                gobject.TYPE_NONE,
                ()),
            'status-updated': (
                gobject.SIGNAL_RUN_LAST,
                gobject.TYPE_NONE,
                ())}

    def __init__(self, avatar, spell_check=True):
        super(UpdateBox, self).__init__(False, 0)

        self._text = gtk.TextView()
        self._text.set_property('wrap-mode', gtk.WRAP_WORD)

        self.avatar = gtk.Image()
        self.avatar.set_from_pixbuf(avatar)

        status_bar = gtk.HBox(False, 0)
        self._update_status = gtk.Label()
        self._update_info = gtk.Label()

        close_button = gtk.Button(stock=gtk.STOCK_CLOSE)
        close_button.set_relief(gtk.RELIEF_NONE)
        close_button.connect('clicked', self.hide)

        add_button = gtk.Button(stock=gtk.STOCK_OK)
        add_button.set_relief(gtk.RELIEF_NONE);
        add_button.connect('clicked', self.do_update)

        status_bar = gtk.HBox(False, 0)
        status_bar.pack_start(self._update_status, expand=True, fill=False,
                padding=0)
        status_bar.pack_start(self._update_info, expand=True, fill=False,
                padding=0)
        status_bar.pack_start(add_button, expand=False, fill=False, padding=0)
        status_bar.pack_start(close_button, expand=False, fill=False,
                padding=0)

        update_bar = gtk.HBox(False, 3)
        update_bar.pack_start(self._text, expand=True, fill=True,
                padding=0)
        update_bar.pack_start(self.avatar, expand=False, fill=False,
                padding=0)

        self.pack_start(status_bar, expand=False, fill=True, padding=0)
        self.pack_start(update_bar, expand=False, fill=True, padding=0)

        # Spell checking the update box
        if spell_check:
            try:
                import gtkspell
                import locale
                language = locale.getlocale()[0]
                self.spell_check = gtkspell.Spell(self._update_text, language)
                _log.debug('Spell checking turned on with language: %s' \
                    % (language))
            except:
                _log.debug('Error initializing spell checking: ' \
                    'spell checking disabled')

        self._text.get_buffer().connect('changed', self._count_chars)
        self._count_chars()     # To show something
        return

    def show(self, reply_to=None):
        """Show the update box and all related widgets."""
        super(UpdateBox, self).show()
        self.show_all()

        if reply_to:
            info = _('(replying to %s)') % (reply_to.username)
            self._update_info.set_text(info)
        else:
            self._update_info.set_text('')

        self._text.grab_focus()
        self.emit('text-focus')
        return

    def hide(self, caller=None):
        """Hide the update box and related widgets."""
        super(UpdateBox, self).hide()
        self.text = ''
        self.hide_all()
        return

    def _count_chars(self, text_buffer=None):
        """Count the number of chars in the edit field and update the
        label that shows the available space."""
        count = len(self.text)

        text = N_('%d character', '%d characters', count) % (count)
        self._update_status.set_text(text)
        return True

    def _get_text(self):
        """Return the text in the update box."""
        text_buffer = self._text.get_buffer()

        start = text_buffer.get_start_iter()
        end = text_buffer.get_end_iter()

        return text_buffer.get_text(start, end, include_hidden_chars=False)

    def _set_text(self, text):
        """Set the textarea text."""
        self._text.get_buffer().set_text(text)
        return
    text = property(_get_text, _set_text)

    def _set_pixbuf(self, pixbuf):
        """Update the avatar pixbuf."""
        _log.debug('Avatar set')
        self.avatar.set_from_pixbuf(pixbuf)
    # pixbuf can only be set, not get.
    pixbuf = property(None, _set_pixbuf)

    def do_update(self, widget=None):
        text = self._text.get_buffer()
        self.emit('status-updated')
        return
