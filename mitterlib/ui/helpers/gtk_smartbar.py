#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a micro-blogging client with multiple interfaces.
# Copyright (C) 2007-2010  Mitter Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gobject
import gtk

import logging

# ----------------------------------------------------------------------
# Constants
# ----------------------------------------------------------------------

_log = logging.getLogger('ui.pygtk')

# ----------------------------------------------------------------------
# Smart statusbar message queueing object
# ----------------------------------------------------------------------
class SmartStatusbar(gtk.Statusbar):
    """A custom status bar with smart placement of messages."""

    def __init__(self):
        super(SmartStatusbar, self).__init__()

        self._static = None
        self._pairs = {}

        self._context = self.get_context_id('Mitter')
        return

    def static(self, message):
        """Place a new static message in the statusbar. Only one static
        message can be in the message stack at a time; if there is any static
        messages in the stack, it will be removed before the new one is
        added."""
        if self._static:
            # remove any previous static messages
            _log.debug('Removing previous static message %d' % (self._static))
            self.remove_message(self._context, self._static)

        self._static = self.push(self._context, message)
        _log.debug('Added new static message %d' % (self._static))
        return

    def volatile(self, message, seconds=10, pair=None):
        """Place a new volatile message in the statusbar. Volatile messages
        are removed from the statusbar in two possiblities:
        * The first way is after the number of seconds has passed.
        * The second way by pairing; The first time a message appears with a
          pair name, it is not queued for removal after some time like normal
          messages; the message will stay in the message stack till its pair
          appear, when it will be finally removed; the second message, in this
          case, will be removed after the number of seconds, like any
          non-paired message."""
        message_id = self.push(self._context, message)

        # pair checking
        if pair:
            if pair in self._pairs:
                # this is the second message. We remove the first one and
                # this one will be removed like any other message.
                first = self._pairs[pair]
                self.remove_message(self._context, first)
                del self._pairs[pair]
                _log.debug('Removed message %d from pair %s' % (first, pair))
            else:
                # this is the first message of the pair. It will not be
                # removed but the timeout.
                self._pairs[pair] = message_id
                _log.debug('added message %d of pair %s' % (message_id, pair))
                return

        self.timeout = gobject.timeout_add(
                seconds * 1000,      # removal after the number of seconds
                self.remove_volatile, message_id)
        return

    def remove_volatile(self, message_id):
        """Kill all messages that are marked as volatiles."""
        _log.debug('Killing volatile message %d' % (message_id))
        self.remove_message(self._context, message_id)
        return
