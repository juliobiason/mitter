#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a Maemo client for Twitter.
# Copyright (C) 2007-2009  The Mitter Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import sys
import logging


def find_image(image_name):
    """Using the image_name, search in the common places. Return the path for
    the image or None if the image couldn't be found."""

    # just because I'm a logging nut

    log = logging.getLogger('mitterlib.find_image')

    # the order is the priority, so keep global paths before local paths

    current_dir = os.path.abspath(os.path.dirname(__file__))

    common_paths = [
            os.path.join(sys.prefix, 'share', 'pixmaps'),
            os.path.join('.', 'pixmaps'),
            os.path.join(current_dir, '..', 'pixmaps')]

    for path in common_paths:
        filename = os.path.join(path, image_name)
        log.debug('Checking %s...' % (filename))
        if os.access(filename, os.F_OK):
            log.debug('Default image is %s' % (filename))
            return filename

    return None
