#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a Maemo client for Twitter.
# Copyright (C) 2007, 2008  Julio Biason, Deepak Sarda
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import mitterlib.ui.helpers.console_utils as console_utils

from mitterlib.network.networkbase import NetworkError
from mitterlib.network import NetworksNoNetworkSetupError, NetworksError

_log = logging.getLogger('ui.tty')


class Interface(object):
    """The console/tty interface for Mitter."""

    NAMESPACE = 'tty'
    PRIORITY = 0

    # -----------------------------------------------------------------------
    # Private functions
    # -----------------------------------------------------------------------
    def _config(self):
        """Set up the networks."""
        options = self._connection.settings()
        console_utils.authorization(options, self._options)
        return

    def _messages(self):
        """Show the latest messages."""
        try:
            data = self._connection.messages()
            console_utils.print_messages(data, self._connection)
        except NetworksNoNetworkSetupError:
            # call the config
            self._config()
        except NetworkError:
            print 'Network failure. Try again in a few minutes.'
        return

    def _replies(self):
        """Show the replies to you."""
        try:
            data = self._connection.replies()
            console_utils.print_messages(data, self._connection)
        except NetworksNoNetworkSetupError:
            # call the config
            self._config()
        except NetworkError:
            print 'Network failure. Try again in a few minutes.'
        return

    def _update(self, message):
        """Update your status."""
        try:
            self._connection.update(message)
        except (NetworksError, NetworkError):
            print 'Error updating your status. Try again in a few minutes.'

        return

    # -----------------------------------------------------------------------
    # Methods required by the main Mitter code
    # -----------------------------------------------------------------------
    def __init__(self, connection, options):
        """Class initialization."""
        self._connection = connection
        self._options = options

    def __call__(self):
        """The callable function, used by mitter to start the interface."""
        status_message = self._options[self.NAMESPACE]['update']
        if status_message:
            self._update(status_message)
            return

        if self._options[self.NAMESPACE]['replies']:
            self._replies()
            return

        self._messages()
        return

    @classmethod
    def options(self, options):
        """Add command line options for this interface."""
        options.add_group(self.NAMESPACE, 'TTY interface')
        options.add_option('--messages',
                group=self.NAMESPACE,
                option='messages',
                help='Display the latest messages.',
                default=False,
                action='store_true',
                is_config_option=False,
                conflict_group='interface')
        options.add_option('--update',
                group=self.NAMESPACE,
                option='update',
                default=None,
                help='Update your status',
                metavar='STATUS',
                type='str',
                is_config_option=False,
                conflict_group='interface')
        options.add_option('--replies',
                group=self.NAMESPACE,
                option='replies',
                help='Get a list of replies instead of the friends timeline',
                default=False,
                action='store_true',
                is_config_option=False,
                conflict_group='interface')
        return
