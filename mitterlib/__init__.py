#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a Maemo client for Twitter.
# Copyright (C) 2007, 2008  Julio Biason, Deepak Sarda
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import glob
import htmlentitydefs


def module_search(source, ignore):
    """Search for modules in the source directory. Ignore any modules
    indicated."""
    base_dir = os.path.dirname(source)

    modules = glob.glob(os.path.join(base_dir, '*.py'))
    # TODO: What if they only have the installed/compiled modules (*.pyc)?
    # Shouldn't we add those to the glob list?
    for module in modules:
        module_name = os.path.basename(module)
        if module_name in ignore:
            # not a usable module
            continue

        # TODO: Maybe this is a good place to test if the module is
        # "importable"
        yield module_name


def htmlize(text):
    """Convert accented characters to their HTML entities."""
    new = []
    # XXX: This is not very effective, but Twitter only accepts 140 chars,
    # so it won't be a big pain.
    for char in text:
        if ord(char) in htmlentitydefs.codepoint2name:
            new.append('&%s;' % (htmlentitydefs.codepoint2name[ord(char)]))
        else:
            new.append(char)
    return ''.join(new)
