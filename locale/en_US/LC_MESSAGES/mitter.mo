��    M      �  g   �      �     �     �     �     �     �     �     
          3     A     V  /   j  #   �     �     �     �     �  A     *   E  c   p  -   �     	     	     	  	   	     "	  &   8	     _	     p	     �	     �	     �	     �	     �	     �	     �	  
   
     
  +   (
     T
     \
     x
  *   �
     �
     �
     �
     �
  )        1     C  2   L  '        �     �     �     �     �     �     
           '     /     6     >  	   D     N     T     Z  	   c     m     s     |     �  	   �     �     �  I  �     �          ,     <     M     Z     o     �     �     �     �  /   �  #   �     #     B     S     c  A   h  *   �  c   �  -   9     g     s     x  	   }     �  &   �     �     �     �                &     4     H     `  
   y     �  +   �     �     �     �  *   �          '     ?     V  )   l     �     �  2   �  '   �               2     ;     @     Y     o     �     �     �     �     �  	   �     �     �     �  	   �     �     �     �     �  	   �     �          A   I                  C   4   J                 ,         2       G       H       =   @                   &                         '      ;         
      	   %       ?       K   0             E                    1   "   M   (              !   L   >   :          .   /   -   5      6   7      9   #   <   *           D   +   )   8              $   B          3             F     &#8212; <i>in reply to %s</i>  &#8212; <i>reposted by %s</i> %d character %s (replying to %s) About Mitter Auto-update disabled Cancel the update Clear the message list Delete a post Delete this message? Deleting message... Disable HTTPS (secure) connection with Twitter. Disable the use of the status icon. Display debugging information. Display messages Display replies Edit Error changing favorite status of the message. Please, try again. Error reposting message. Please try again. Error retrieving current messages. Auto-refresh disabled. Use the "Refresh" option to re-enable it. Error updating your status. Please try again. Exit Mitter File Help Interface Interface to be used. Marking message from %s as favorite... Message deleted. Message favorited. Message related options Message reposted Message unfavorited. Messages (%d) Mitter project page New messages retrieved. Next update in %d minute Open on %s Password Put someone's else message on your timeline Re_post Refresh interval (minutes): Refresh interval. Removing message from %s from favorites... Replies (%d) Reposting %s message... Retrieving messages... Retrieving replies... Send a response to someone's else message Sending update... Settings Sorry, no interface could be found for your system Toggle the favorite status of a message Update the listing Update your status Username View Your status was updated. Your twitter password Your twitter username _About _Cancel _Clear _Delete _Edit _Favorite _File _Help _Message _Messages _Quit _Refresh _Replies _Reply _Settings _Update _View Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-10 10:24-0200
PO-Revision-Date: 2010-01-10 10:33-0200
Last-Translator: Julio Biason <>
Language-Team: English
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  &#8212; <i>in reply to %s</i>  &#8212; <i>reposted by %s</i> %d character %s (replying to %s) About Mitter Auto-update disabled Cancel the update Clear the message list Delete a post Delete this message? Deleting message... Disable HTTPS (secure) connection with Twitter. Disable the use of the status icon. Display debugging information. Display messages Display replies Edit Error changing favorite status of the message. Please, try again. Error reposting message. Please try again. Error retrieving current messages. Auto-refresh disabled. Use the "Refresh" option to re-enable it. Error updating your status. Please try again. Exit Mitter File Help Interface Interface to be used. Marking message from %s as favorite... Message deleted. Message favorited. Message related options Message reposted Message unfavorited. Messages (%d) Mitter project page New messages retrieved. Next update in %d minute Open on %s Password Put someone's else message on your timeline Re_post Refresh interval (minutes): Refresh interval. Removing message from %s from favorites... Replies (%d) Reposting %s message... Retrieving messages... Retrieving replies... Send a response to someone's else message Sending update... Settings Sorry, no interface could be found for your system Toggle the favorite status of a message Update the listing Update your status Username View Your status was updated. Your twitter password Your twitter username _About _Cancel _Clear _Delete _Edit _Favorite _File _Help _Message _Messages _Quit _Refresh _Replies _Reply _Settings _Update _View 