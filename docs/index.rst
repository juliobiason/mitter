.. Mitter documentation master file, created by sphinx-quickstart on Sun Apr  5 18:19:25 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mitter's documentation!
==================================

User Documentation

.. toctree::
   :maxdepth: 1

   README
   INSTALL
   HACKING
   CHEAT-CODES


Developer Documentation:

.. toctree::
   :maxdepth: 2

   networkbase

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

