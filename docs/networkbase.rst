:mod:`networkbase` -- Base classes for all networks
===================================================

.. automodule:: networkbase
   :members:

NetworkUser -- Unified information about users
----------------------------------------------

.. autoclass:: NetworkUser
   :members:

NetworkData -- Unified information about messages
-------------------------------------------------

.. autoclass:: NetworkData
   :members:

NetworkBase -- Base class for all networks
------------------------------------------

.. autoclass:: NetworkBase
   :members:
