#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mitter, a Maemo client for Twitter.
# Copyright (C) 2007, 2008  Julio Biason
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import urllib
import sys
import os
import os.path
import logging
import warnings
import gettext

import mitterlib.network as network

from mitterlib.configopt import ConfigOpt
from mitterlib.network import Networks
from mitterlib.ui import Interfaces

log = logging.getLogger('mitter')

# ----------------------------------------------------------------------
# i18n stuff
# ----------------------------------------------------------------------
local_path = os.path.realpath(os.path.dirname(sys.argv[0]))

gettext.bindtextdomain('mitter', local_path)
gettext.textdomain('mitter')

lang = gettext.translation('mitter', local_path, fallback = True)
_ = lang.gettext

def main():
    """Main function."""

    # options

    options = ConfigOpt()
    options.add_group('General', ('General options'))
    options.add_option('-d', '--debug',
            group='General',
            option='debug',
            action='store_true',
            help=_('Display debugging information.'),
            default=False,
            is_config_option=False)
    options.add_option('-i', '--interface',
            group='General',
            option='interface',
            default=None,
            metavar='INTERFACE',
            help=_('Interface to be used.'))

    # Start the network manager (which will add the network options)
    connection = Networks(options)

    # Start the interface manager and make interfaces add their options to the
    # command line
    interfaces = Interfaces(options)
    interfaces.options()

    # Parse the command line options and the config file
    options()

    # start the logging service
    if options['General']['debug']:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('mitter')

    # disable the warnings. Interfaces may choose to receive the warnings
    # changing the filter to "error"
    warnings.simplefilter('ignore')

    # select an interface (preferably, the one the user selected in the
    # command line)
    preferred_interface = options['General']['interface']
    log.debug('Config interface: %s', preferred_interface)

    if 'interface' in options.conflicts:
        preferred_interface = options.conflicts['interface']
        log.debug('Command line interface: %s', preferred_interface)

    # load the module with the selected interface (or pick one for the user if
    # they don't chose anything.)
    display = interfaces.load(connection, preferred_interface)
    if display is None:
        log.error(_('Sorry, no interface could be found for your system'))
        return

    # display the interface (the interface should take care of updating
    # itself)
    display()
    options.save()

if __name__ == '__main__':
    main()
