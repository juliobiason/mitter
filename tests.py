#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import logging
import cgi

from mitterlib.network import twitter
from mitterlib.configopt import ConfigOpt


def _request(self, resource, headers=None, body=None):
    logging.debug('Resource: %s')
    logging.debug('Headers: %s')
    logging.debug('body: %s')

    return {}


def _request_update(self, resource, headers=None, body=None):
    """Monkey-patch request for update. Returns a random dictionary for
    NetworkData."""
    # body = urllib.urlencode(dict)
    body = cgi.parse_qs(body)
    result = {
        'id': 1,
        'user': {
            'name': 'Test',
            'screen_name': 'test',
            'profile_image_url': None},
        'created_at': 'Tue Mar 13 00:12:41 +0000 2007',
        'text': body['status'][0]} # Go figure...
    return result


class TwitterEncodingTests(unittest.TestCase):

    def setUp(self):
        # Generate a set of options required for starting the Twitter
        # connection.
        # we don't call options(), so it won't load the options in the config
        # file. If tests need to test specific options, they can change
        # options directly.
        options = ConfigOpt()
        twitter.Connection.options(options)
        self.connection = twitter.Connection(options)

    def test_twitter_unhtml(self):
        """Test the _unhtml() function inside the Twitter."""

        text = u'RT @fisl10: Abertas as inscri&ccedil;&otilde;es para o ' \
                'fisl10 http://tinyurl.com/cqrdsc'
        result = u'RT @fisl10: Abertas as inscrições para o fisl10 ' \
                'http://tinyurl.com/cqrdsc'

        self.assertEqual(result, twitter._unhtml(text))

    def test_unicode(self):
        """Test if sending unicode messages breaks the system."""
        text = 'fisl10: Abertas as inscrições para o fisl10 ' \
                'http://tinyurl.com/cqrdsc'

        twitter.Connection._request = _request_update
        try:
            self.connection.update(text)
        except UnicodeEncodeError:
            self.fail('UnicodeEncodeError')

    def test_encoding(self):
        """Test the UTF8 encoding on Twitter."""
        text = u'À'
        result = twitter.htmlize(text)
        expected = '%25C3%2580'
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
