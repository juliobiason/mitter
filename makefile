deb:
	dpkg-buildpackage -us -uc

doc:
	cd docs
	make html

pot:
	xgettext --language=Python --keyword=_ --keyword=N_ --output=i18n/mitter.pot mitter `find . -name '*.py'`

check:
	find . -name '*.py' -exec pyflakes {} \;
	python utils/pep8.py --filename=*.py --repeat .

sdist:
	python setup.py sdist --formats=gztar,zip

.PHONY: check pot doc deb sdist
